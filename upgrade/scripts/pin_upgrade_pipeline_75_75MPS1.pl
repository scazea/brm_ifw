#!/usr/bin/env perl
#
# @(#)$Id: pin_upgrade_pipeline_75_75MPS1.pl /cgbubrm_7.5.0.13.0custfixportalbase.m1/1 2015/08/03 02:48:47 mkothari Exp $
#
# $Header: bus_platform_vob/upgrade/Integrate/75_75ps13/oracle/pin_upgrade_pipeline_75_75MPS1.pl /cgbubrm_7.5.0.13.0custfixportalbase.m1/1 2015/08/03 02:48:47 mkothari Exp $
#
# pin_upgrade_pipeline_75_75MPS1.pl
#
# Copyright (c) 2009, 2015, Oracle and/or its affiliates. All rights reserved.
#
#    NAME
#      pin_upgrade_pipeline_75_75MPS1.pl - this script is the main driver file
#      for upgrading the BRM PIPELINE DB to 75 MPS1
#
#    DESCRIPTION
#      This script will call the exposed API pipeline_db_upgrade_75_() which will do the Pipeline Upgrade.
#
#    NOTES
#
#      Here are the details of the Function which are called by the Driver Perl File.
#      pipeline_db_upgrade_75(): This Subrotine will call the function upgrade_pipeline_db from within.
#      This function , upgrade_pipeline_db() & modify_columns_75(), which will result in upgrading the Pipeline Database.
#      The upgrade_pipeline_db()  & modify_columns_75() will return an execution result value as part of its execution.
#
#	Error Logging
#	----------------
#	Error logging will happen in the following file
#       "$IFW_HOME/log/pin_upgrade_pipeline_75_75MPS1.log"
#
#    MODIFIED   (MM/DD/YY)
#    mkothari    08/01/15 - Creation
#======================================================================

use strict;
use warnings;
use Env;

our $IFW_UPGRADE_DIR = "$IFW_HOME/upgrade/scripts";
our $IFW_LOG_DIR     = "$IFW_HOME/log";

require "$IFW_UPGRADE_DIR/pin_res.pl";
require "$IFW_UPGRADE_DIR/pin_functions.pl";
require "$IFW_UPGRADE_DIR/pin_oracle_functions.pl";
require "$IFW_HOME/upgrade/pipeline_upgrade.cfg";
require "$IFW_UPGRADE_DIR/pipeline_tables.cfg";
require "$IFW_UPGRADE_DIR/modify_column_75.pl";
require "$IFW_UPGRADE_DIR/modify_column_75ps1.pl";
require "$IFW_UPGRADE_DIR/pipeline_schema_update_75ps2.pl";
require "$IFW_UPGRADE_DIR/modify_schema_75ps6.pl";
require "$IFW_UPGRADE_DIR/modify_schema_75ps7.pl";

our %MAIN_DM;
our %MAIN_CM;

our %CM;
our %DM;
our $ret_val = -1;

%DM = %MAIN_DM;
%CM = %MAIN_CM;


our @EXECUTE_ALL_PIPELINE_UPGRADE_SCRIPTS = ("modify_columns_75",
					     "modify_columns_75ps1",
					     "modify_columns_75ps2",
					     "modify_columns_75ps6",
					     "modify_columns_75ps7");

print "now executing pipeline upgrade module ... \n";

	$ret_val = &pipeline_db_upgrade_75();

	if ( $ret_val == 0 ) {
		print "pipeline upgrade to BRM 7.5 MPS1 is successful. \n";
		print
	"please see the pipeline upgrade log file at $IFW_LOG_DIR/pin_upgrade_pipeline_75_75MPS1.log \n";
		print "################################################ \n";
	} elsif ( $ret_val == 2 ) {
		print
	"there are no function apis to process. Pipeline upgrades will not continue. \n";
		print
	"please see the pipeline upgrade log file at $IFW_LOG_DIR/pin_upgrade_pipeline_75_75MPS1.log \n";
		print "################################################ \n";
	} else {
		print "the pipeline upgrade did not happen successfully \n";
		print
	"please see the pipeline upgrade log file at $IFW_LOG_DIR/pin_upgrade_pipeline_75_75MPS1.log \n";
		print "################################################ \n";  
	}

sub pipeline_db_upgrade_75 {
	my $upgrade_api = "";
	my $firstelem   = $EXECUTE_ALL_PIPELINE_UPGRADE_SCRIPTS[0];
	my $api_result  = -1;
	my $Result      = -1;
	my $pipelineupgrade75pinlog = "$IFW_LOG_DIR/pin_upgrade_pipeline_75_75MPS1.log";
	my $writestatement = "";

	open( TMPFILE1, ">$pipelineupgrade75pinlog" )
	    || die "cannot write into $pipelineupgrade75pinlog \n";

	if ( $firstelem eq "" ) {
		$Result         = 2;
		$writestatement = "There are no functions to process \n";
		print TMPFILE1 $writestatement;
		close(TMPFILE1);
	} else {
		foreach $upgrade_api (@EXECUTE_ALL_PIPELINE_UPGRADE_SCRIPTS) {
                        print "################################################ \n";
                    
			print "pipeline upgrade from 7.5 MPS1 is now executing api:$upgrade_api \n";

			$api_result = eval($upgrade_api);
			if ( $api_result == 0 ) {
				$Result = 0;
				$writestatement = "Successfully executed the function $upgrade_api";
				open( TMPFILE1, ">>$pipelineupgrade75pinlog " )
				    || die
				    "cannot read $pipelineupgrade75pinlog \n";
				print TMPFILE1 $writestatement;
				close(TMPFILE1);
                                print "################################################ \n";
				next;
			} else {
				$Result = $api_result;
				open( TMPFILE1, ">>$pipelineupgrade75pinlog " )
				    || die
				    "cannot read $pipelineupgrade75pinlog \n";
				print TMPFILE1 "execution failed for $upgrade_api, Please see the log file at $IFW_LOG_DIR. \n";
				close(TMPFILE1);
                                print "################################################ \n";
				last;
			}

		}

	}

	return $Result;
}
1;
